import { v4 as uuidv4 } from 'uuid';


export const getUsers = async ( url ) => {

    try {
        
        const resp = await fetch(url);
        const data = await resp.json();

        return data;
        
    } catch (error) {
        console.log(error);
        return false;
    }

};

export const deletePost = async ( id ) => {

    try {
        const resp = await fetch(`https://reqres.in/api/users/${ id }`);
        return resp.ok;
    } catch (error) {
        console.log(error)
        return false;
    }
    
};

export const createPost = async ( url, info ) => {

    try {

        const resp = await fetch(url, {
            method: 'POST',
            headers: { 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify({
                id: uuidv4(),
                email: info.email,
                first_name: info.name,
                last_name: info.lastName,
            }),
    
        });
    
        const data = await resp.json();
        return data;
    } catch (error) {
        console.log(error);
        return false;
    }

};

export const updatePost = async ( url, info ) => {

    try {
        const resp = await fetch(url, {
            method: 'PUT',
            body: JSON.stringify(info),
            headers: { 'Content-type': 'application/json; charset=UTF-8'}
        });
    
        const data = await resp.json();
        return data;
    } catch (error) {
        console.log(error);
        return false;
    }

};