import React, { useContext, useState } from 'react';
import { useForm } from '../hooks/useForm';
import { createPost } from './helpers/fetch';
import Swal from 'sweetalert2';
import { PostContext } from './context/PostContext';
import { types } from '../types/types';

export const FormScreen = () => {

    const [ loading, setLoading ] = useState(false);
    const { dispatch } = useContext(PostContext);

    const [ formValues, handleInputChange ] = useForm({ name: '', lastName: '', email: '' });
    const { name, lastName, email } = formValues;
    
    const handleSubmit = async (e) => {
        e.preventDefault();

        if(name.trim().length === 0 || lastName.trim().length === 0 || email.trim().length === 0) {
            Swal.fire('All fields are required', '', 'warning');
            return;
        }

        setLoading(true);
        const data = await createPost('https://reqres.in/api/users', formValues );
        
        if(!data) {
            Swal.fire('We have a problem', '', 'error');
            setLoading(false);
            return;
        }

        Swal.fire('Success', '', 'success');
        setLoading(false);

        dispatch({
            type: types.addPost,
            payload: data
        })
        
    }

    return (
        <div className="container mt-3 animate__animated animate__fadeIn">
        <h3>Create post</h3>
        <form onSubmit={ handleSubmit }>
            <div className="form-group mt-3">
                <label htmlFor="name"><b>Name</b></label>
                <input 
                    type="text" 
                    className="form-control mt-3"  
                    name="name"
                    onChange={ handleInputChange }  
                    value={ name }  
                />
            </div>
            <div className="form-group mt-3">
                <label htmlFor="lastName"><b>Last Name</b></label>
                <input 
                    type="text" 
                    className="form-control mt-3"  
                    name="lastName"
                    onChange={ handleInputChange }
                    value={ lastName }  
                />
            </div>
            <div className="form-group mt-3">
                <label htmlFor="email"><b>Email</b></label>
                <input 
                    type="email" 
                    className="form-control mt-3"  
                    name="email"
                    onChange={ handleInputChange }
                    value={ email }  
                />
            </div>
            <button type="submit" className="btn btn-primary mt-3" disabled={ loading }>Confirm</button>
        </form>
        </div>
    )
}
