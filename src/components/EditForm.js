import React, { useContext, useMemo, useState } from 'react'
import { useParams } from 'react-router';
import { updatePost } from './helpers/fetch';
import { useForm } from '../hooks/useForm';
import { PostContext } from './context/PostContext';
import { getPostById } from './helpers/getPostById';
import Swal from 'sweetalert2';

export const EditForm = ({ history }) => {

    const [ loading, setLoading ] = useState(false);
    const { state } = useContext(PostContext);

    const { id } = useParams();

    const post = useMemo(() => getPostById(state, id), [state, id]);   

    const [ formValues, handleInputChange ] = useForm({ name: post.first_name, lastName: post.last_name, });
    const { name, lastName } = formValues;
    
    const handleReturn = () => {

        if( history.length <= 2 ) {
            history.push('/home');
        } else {
            history.goBack();
        }

    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if ( name.trim().length === 0 || lastName.trim().length === 0 ) {
            Swal.fire('All fields are required', '', 'warning');
            return;
        }

        setLoading(true);
        const data = await updatePost(`https://reqres.in/api/users/${ id }`);

        if (!data) {
            Swal.fire('We have a problem', '', 'error');
            setLoading(false);
            return;
        }

        post.first_name = name;
        post.last_name = lastName
        Swal.fire( 'Success!' , '', 'success');
        setLoading(false);
    };

    return (
        <div className="container mt-3 animate__animated animate__fadeIn">
            <h3>Edit post</h3>
            <form onSubmit={ handleSubmit }>
                <div className="form-group mt-3">
                    <label htmlFor="name"><b>Name</b></label>
                    <input 
                        type="text" 
                        className="form-control mt-2"  
                        name="name"
                        onChange={ handleInputChange }  
                        value={ name }  
                    />
                </div>
                <div className="form-group mt-3">
                    <label htmlFor="lastName"><b>last Name</b></label>
                    <input 
                        type="text" 
                        className="form-control mt-2"  
                        name="lastName"
                        onChange={ handleInputChange }  
                        value={ lastName }  
                    />
                </div>
                <button type="submit" className="btn btn-primary mt-3" disabled={ loading }>Confirm</button>
            </form>

            <button className="btn btn-outline-info w-25" onClick={ handleReturn }>
                Return
            </button>

        </div>
    )
}
