import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { types } from '../../types/types';
import { PostContext } from '../context/PostContext';
import { deletePost } from '../helpers/fetch';
import { getPostsFiltered } from '../helpers/getPostsFiltered';

export const TitlesList = ({ name, id, email }) => {

    const { state, dispatch } = useContext(PostContext);
    const [ loading, setLoading ] = useState(false);

    const handleDelete = async () => {
        setLoading(true);
        
        const resp = await deletePost(id);
        const posts = getPostsFiltered( state, id );
        if(resp){
            dispatch({
                type: types.deletePost,
                payload: posts
            });
            Swal.fire('Success', '', 'success');
        } else if(!resp) {
            dispatch({
                type: types.deletePost,
                payload: posts
            });
            Swal.fire("This don't exist in our database", '', 'info');
        }
        setLoading(false);
    };

    return (
        <div className="card-deck carta">
            <div>
                <h5 className="card-title title">{name}</h5>
                <p>{email}</p>
            </div>
            <Link to={`edit/${id}`} className="btn btn-primary">
                Edit
                    </Link>
            <Link to={`detail/${id}`} className="btn btn-dark">
                Details
            </Link>
            <button onClick={handleDelete} className="btn btn-danger" disabled={loading}>
                Delete
            </button>
        </div>
    )
}
