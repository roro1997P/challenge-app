import React, { useContext } from 'react'
import { PostContext } from '../context/PostContext'
import { TitlesList } from './TitlesList';
import { types } from '../../types/types';
import { getUsers } from '../helpers/fetch';

export const HomeScreen = () => {

    const { state, dispatch } = useContext(PostContext);

    const handleClick = async () => {
        const { data } = await getUsers('https://reqres.in/api/users');
        console.log(data)
        dispatch({
            type: types.getPosts,
            payload: data
        })
    };

    return (
        <div className="container">
        {
            (state.length === 0 )
                ? <button onClick={ handleClick } className="btn btn-info mt-5 align">Get Posts</button>
                : <ul className="cartas animate__animated animate__fadeIn">
                        {
                            state.map(p => (
                                <TitlesList
                                    key={p.id}
                                    name={p.first_name}
                                    id={p.id}
                                    email={p.email}
                                />
                            ))
                        }

                    </ul>
        }
        </div>
    )
}
